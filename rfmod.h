#ifndef RFMUX_H
#define RFMUX_H

#define	BUFFER_TS_PACKETS	10
#define TS_PACKET_SIZE		188
#define	MAX_PACKET_SIZE		(BUFFER_TS_PACKETS * TS_PACKET_SIZE)
#define SOCKET_BUFFER           (1024*1024)

#define RTP_PT_MP2T             33              /* RFC2250 */

#define RTP_PT_OFF              1
#define RTP_VERSION_OFF         0
#define RTP_SEQ_OFF             2
#define RTP_TSTAMP_OFF          4
#define RTP_SSRC_OFF            8

#define RTP_HEADROOM            12

#define DVB_PID_MAX             0x2000
#define SECTION_SIZE            4096

#define DVB_SYNC                0x47

#define DEBUG

#ifdef DEBUG
#define dprintf		printf
#else
#define dprintf( a... )
#endif

#endif
