extern int loglevel;


/*
 *
 * logging
 *
 *
 */
void logwrite_inc_level(void );
void logwrite_dec_level(void );
void logwrite(int level, const char *format, ...);
