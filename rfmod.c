#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <poll.h>
#include <unistd.h>
#include <getopt.h>
#include <sched.h>
#include <syslog.h>
#include <signal.h>
#include <errno.h>
#include <pthread.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>

#include "rfmod.h"
#include "logging.h"
#include "socket.h"

#include "daemon.h"

#include "api.h"

#define	MY_NAME	"rfmod"

static int DevNo = 0;
static int DevType = EAGLEII;


static void usage(char *argv[]);
static void ts_output(unsigned char *p, int total);
static int init_modulator(
	int frequency, 
	int bandwidth, 
	int gain, 
	int constellation, 
	int codeRate, 
	int guardInterval, 
	int transmissionMode, 
	int cellid
);


static void usage(char *argv[])
{
	fprintf(stderr, "Usage: %s [-v] [-d] [-T deviceType] [-D deviceIndex] [-o timeout] [-S stamp_packets] [-F fequencykHz] [-g gain] [-B bandwidth] [-M transmissionMode] [-C connstellation] [-R codeRate] [-G guardInterval] [-L cellid] [-p port] [-f interface] ip_addr[:port]\n", argv[0]);
	exit(EXIT_FAILURE);
}

static void sigsegv_handler(int sig, siginfo_t *si, void *unused)
{
	logwrite(LOG_ERR, "Got SIGSEGV at address 0x%08x", (long)si->si_addr);
	exit(EXIT_FAILURE);
}

static void sigusr1_handler(int sig, siginfo_t *si, void *unused)
{
	logwrite(LOG_ERR, "Increment log verbosity.");
	logwrite_inc_level();
}


static void sigusr2_handler(int sig, siginfo_t *si, void *unused)
{
	logwrite(LOG_ERR, "Decrement log verbosity.");
	logwrite_dec_level();
}

int main(int argc, char *argv[])
{
	int fd;
	int opt;
	int nodaemon = 0;
	int timeout = 1000; /* 1 second */
	int stamp = 1701900; /* 15 minutes for 1891 packets per second */

	char *remoteaddr = "";
	int remoteport = 1234;
        struct sched_param schedParam;
	struct pollfd fds;
	char *colon;

	int port = 1234;
	char *fromIf = NULL;
	int fromIfIndex = 0;

	int frequency = 730000;
	int bandwidth = 8000;
	int gain = -47;

	int constellation = 2;
	int codeRate = 1;
	int guardInterval = 3;
	int transmissionMode = 1;
	int cellid = 0;

	int seq;
	char *inspec = "";
	
	int buffer = 2*1024*1024;

	openlog(MY_NAME, LOG_PID, LOG_LOCAL5);
	logwrite(LOG_INFO, "starting");

	while ((opt = getopt(argc, argv, "b:dvT:D:S:o:f:p:F:B:C:R:D:G:M:L:g:")) != -1) {
		switch (opt) {
		/* GENERAL OPTIONS */
		case 'S':
			stamp = atoi(optarg);
			break;
		case 'o':
			timeout = atoi(optarg);
			break;
		case 'v':
			logwrite_inc_level();
			break;

		case 'd':
			nodaemon = 1;
			break;

		case 'p':
			port = strtol(optarg, NULL, 0);
			break;

		case 'T':
			DevType = strtol(optarg, NULL, 0);
			break;

		case 'D':
			DevNo = strtol(optarg, NULL, 0);
			break;

		/* OUTPUT CONTROL */
		case 'g':
			gain = strtol(optarg, (char **)NULL, 0);
			break;

		case 'F':
			frequency = strtol(optarg, (char **)NULL, 0);
			break;
		case 'B':
			bandwidth = strtol(optarg, (char **)NULL, 0);
			break;
		case 'C':
			constellation = strtol(optarg, (char **)NULL, 0);
			break;
		case 'R':
			codeRate = strtol(optarg, (char **)NULL, 0);
			break;
		case 'G':
			guardInterval = strtol(optarg, (char **)NULL, 0);
			break;
		case 'M':
			transmissionMode = strtol(optarg, (char **)NULL, 0);
			break;
		case 'L':
			cellid = strtol(optarg, (char **)NULL, 0);
			break;

		/* INPUT CONTROL */
		case 'b':
			buffer = strtol(optarg, (char **)NULL, 0);
			break;
		case 'f':
			fromIf = strdup(optarg);
			fromIfIndex = if_nametoindex(fromIf);
			if (!fromIfIndex) {
				logwrite(LOG_ERR, "Interface '%s' does not exist", fromIf);
			}
			break;

		default: /* '?' */
			fprintf(stderr, "Bad parameter %c\n", opt);
			usage(argv);
		}
	}

	if (argc-optind != 1) {
		fprintf(stderr, "Bad parameters %d\n", argc-optind);
		for (opt = optind; opt < argc; opt++) {
			fprintf(stderr, "arg %d: %s\n", opt, argv[opt]);
		}
		usage(argv);
	}

	optarg = argv[optind];
	seq = -1;
	inspec = optarg;
	colon = index(optarg, ':');

	if (colon) {
		remoteaddr = strndup(optarg, colon-optarg);
		remoteport = strtol(colon+1, NULL, 0);
	} else {
		remoteaddr = optarg;
		remoteport = port;
	}

        schedParam.sched_priority = sched_get_priority_max(SCHED_RR)/2-1;
        if (pthread_setschedparam(pthread_self(), SCHED_RR, &schedParam)) {
                logwrite(LOG_INFO, "setschedparam failed - running at normal priority");
        }

	/* init inputs */
	logwrite(LOG_INFO, "inspec=[%s], remoteaddr=[%s], remoteport=%d", inspec, remoteaddr, remoteport);
	fd = socket_open(remoteaddr, remoteport, fromIf, fromIfIndex, buffer, timeout);
	if (fd < 0) {
		logwrite(LOG_ERR, "Open socket %s: %d", inspec, errno);
		return 1;
	}
	fds.fd = fd;
	fds.events = POLLIN|POLLERR|POLLHUP;


	/* init output */
	init_modulator(frequency, bandwidth, gain, constellation, codeRate, guardInterval, transmissionMode, cellid);

	/* now become a daemon */
	if (!nodaemon) {
		daemonize(NULL, NULL);
	}
	logwrite(LOG_ERR, "RESTART");

	{
		struct sigaction sa;
		sa.sa_flags = SA_SIGINFO | SA_RESTART;
		sigemptyset(&sa.sa_mask);
		sa.sa_sigaction = sigsegv_handler;
		if (sigaction(SIGSEGV, &sa, NULL) == -1) {
			logwrite(LOG_ERR, "Failed to set SIGSEGV handler");
		}
		sa.sa_flags = SA_SIGINFO | SA_RESTART;
		sigemptyset(&sa.sa_mask);
		sa.sa_sigaction = sigusr1_handler;
		if (sigaction(SIGUSR1, &sa, NULL) == -1) {
			logwrite(LOG_ERR, "Failed to set SIGUSR1 handler");
		}
		sa.sa_flags = SA_SIGINFO | SA_RESTART;
		sigemptyset(&sa.sa_mask);
		sa.sa_sigaction = sigusr2_handler;
		if (sigaction(SIGUSR2, &sa, NULL) == -1) {
			logwrite(LOG_ERR, "Failed to set SIGUSR2 handler");
		}
	}

	int n = 0;
	for (;;) {
		int len;
		int ret;

		if (n++ > stamp) {
			logwrite(LOG_DEBUG, "tick");
			n = 0;
		}
		ret = poll(&fds, 1, timeout);
		if (ret > 0) {
			if (fds.revents & POLLIN) {
				unsigned char p[MAX_PACKET_SIZE];

				len = read(fds.fd, p, sizeof(p));
				if (len < 0) {
					logwrite(LOG_ERR, "Receiving from socket %s: %d", inspec, errno);
					break;
				}

				if (len > RTP_HEADROOM && p[RTP_VERSION_OFF] == 0x80 && p[RTP_PT_OFF] == RTP_PT_MP2T) {
					static unsigned long good = 0;
					uint16_t rseq = (p[RTP_SEQ_OFF] << 8)+p[RTP_SEQ_OFF+1];
					if (seq >= 0 && seq != rseq) {
						logwrite(LOG_INFO, "%s %lu RTP seq %d err: expected %u != received %u", inspec, good, rseq-seq, seq, rseq);
						good = 0;
					} else {
						good++;
					}
					seq = (rseq+1) & 0xFFFF;
					/* skip RTP header */
					len -= RTP_HEADROOM;
					memmove(p, p+RTP_HEADROOM, len);
				}
				/* send */
				ts_output(p, len);
			}
			if (fds.revents & POLLHUP) {
				logwrite(LOG_ERR, "remote %s died", inspec);
			}
			if (fds.revents & POLLERR) {
				logwrite(LOG_ERR, "remote %s error", inspec);
			}
		} else if (ret == 0) {
			logwrite(LOG_INFO, "Timeout");
		} else if (errno != EINTR) {
			logwrite(LOG_ERR, "poll failed: %d", errno);
		}
	}
	logwrite(LOG_NOTICE, "terminated");
	closelog();
	return 0;
}

static Byte UT100_IQtable[] = {
    0x20, 0x49, 0x51, 0x20, 0x54, 0x61, 0x62, 0x6c, 0x65, 0x20, 0x33, 0x00, 0x00, 0x00, 0x00, 0x5b,
    0x50, 0xc3, 0x00, 0x00, 0x2d, 0x00, 0xf2, 0xfb, 0x60, 0xea, 0x00, 0x00, 0x11, 0x00, 0x68, 0xfd,
    0x70, 0x11, 0x01, 0x00, 0x03, 0x00, 0xdd, 0xfe, 0x80, 0x38, 0x01, 0x00, 0x00, 0x00, 0x52, 0x00,
    0x90, 0x5f, 0x01, 0x00, 0x06, 0x00, 0xa3, 0x01, 0xa0, 0x86, 0x01, 0x00, 0x17, 0x00, 0x06, 0x03,
    0xb0, 0xad, 0x01, 0x00, 0x1a, 0x00, 0x21, 0x03, 0xc0, 0xd4, 0x01, 0x00, 0x16, 0x00, 0xe1, 0x02,
    0xd0, 0xfb, 0x01, 0x00, 0x11, 0x00, 0x8f, 0x02, 0xe0, 0x22, 0x02, 0x00, 0x0c, 0x00, 0x34, 0x02,
    0xf0, 0x49, 0x02, 0x00, 0x09, 0x00, 0xd9, 0x01, 0x00, 0x71, 0x02, 0x00, 0x06, 0x00, 0x7e, 0x01,
    0x10, 0x98, 0x02, 0x00, 0x04, 0x00, 0x2c, 0x01, 0x20, 0xbf, 0x02, 0x00, 0x02, 0x00, 0xda, 0x00,
    0x30, 0xe6, 0x02, 0x00, 0x01, 0x00, 0x92, 0x00, 0x40, 0x0d, 0x03, 0x00, 0x01, 0x00, 0x52, 0x00,
    0x50, 0x34, 0x03, 0x00, 0x01, 0x00, 0x00, 0x00, 0x60, 0x5b, 0x03, 0x00, 0x01, 0x00, 0xc9, 0xff,
    0x70, 0x82, 0x03, 0x00, 0x01, 0x00, 0x93, 0xff, 0x80, 0xa9, 0x03, 0x00, 0x02, 0x00, 0x65, 0xff,
    0x90, 0xd0, 0x03, 0x00, 0x02, 0x00, 0x38, 0xff, 0xa0, 0xf7, 0x03, 0x00, 0x03, 0x00, 0x13, 0xff,
    0xb0, 0x1e, 0x04, 0x00, 0x03, 0x00, 0xef, 0xfe, 0xc0, 0x45, 0x04, 0x00, 0x04, 0x00, 0xd4, 0xfe,
    0xd0, 0x6c, 0x04, 0x00, 0x04, 0x00, 0xc1, 0xfe, 0xe0, 0x93, 0x04, 0x00, 0x05, 0x00, 0xaf, 0xfe,
    0xf0, 0xba, 0x04, 0x00, 0x05, 0x00, 0xaf, 0xfe, 0x00, 0xe2, 0x04, 0x00, 0x04, 0x00, 0xa6, 0xfe,
    0x10, 0x09, 0x05, 0x00, 0x04, 0x00, 0xaf, 0xfe, 0x20, 0x30, 0x05, 0x00, 0x04, 0x00, 0xb8, 0xfe,
    0x30, 0x57, 0x05, 0x00, 0x04, 0x00, 0xc1, 0xfe, 0x40, 0x7e, 0x05, 0x00, 0x04, 0x00, 0xd4, 0xfe,
    0x50, 0xa5, 0x05, 0x00, 0x03, 0x00, 0xe6, 0xfe, 0x60, 0xcc, 0x05, 0x00, 0x02, 0x00, 0xef, 0xfe,
    0x70, 0xf3, 0x05, 0x00, 0x02, 0x00, 0x13, 0xff, 0x80, 0x1a, 0x06, 0x00, 0x01, 0x00, 0x1c, 0xff,
    0x90, 0x41, 0x06, 0x00, 0x01, 0x00, 0x41, 0xff, 0xa0, 0x68, 0x06, 0x00, 0x01, 0x00, 0x53, 0xff,
    0xb0, 0x8f, 0x06, 0x00, 0x00, 0x00, 0x6e, 0xff, 0xc0, 0xb6, 0x06, 0x00, 0x00, 0x00, 0x8a, 0xff,
    0xd0, 0xdd, 0x06, 0x00, 0x00, 0x00, 0xa5, 0xff, 0xe0, 0x04, 0x07, 0x00, 0x00, 0x00, 0xb7, 0xff,
    0xf0, 0x2b, 0x07, 0x00, 0x00, 0x00, 0xc9, 0xff, 0x00, 0x53, 0x07, 0x00, 0x00, 0x00, 0xdc, 0xff,
    0x10, 0x7a, 0x07, 0x00, 0x00, 0x00, 0xdc, 0xff, 0x20, 0xa1, 0x07, 0x00, 0xff, 0xff, 0xee, 0xff,
    0x30, 0xc8, 0x07, 0x00, 0x00, 0x00, 0xf7, 0xff, 0x40, 0xef, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x50, 0x16, 0x08, 0x00, 0xff, 0xff, 0x00, 0x00, 0x60, 0x3d, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x70, 0x64, 0x08, 0x00, 0xff, 0xff, 0xf7, 0xff, 0x80, 0x8b, 0x08, 0x00, 0xff, 0xff, 0x12, 0x00,
    0x90, 0xb2, 0x08, 0x00, 0xff, 0xff, 0xf7, 0xff, 0xa0, 0xd9, 0x08, 0x00, 0x00, 0x00, 0x09, 0x00,
    0xb0, 0x00, 0x09, 0x00, 0x00, 0x00, 0x12, 0x00, 0xc0, 0x27, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00,
    0xd0, 0x4e, 0x09, 0x00, 0x00, 0x00, 0x12, 0x00, 0xe0, 0x75, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00,
    0xf0, 0x9c, 0x09, 0x00, 0xff, 0xff, 0x00, 0x00, 0x00, 0xc4, 0x09, 0x00, 0x00, 0x00, 0xf7, 0xff,
    0x10, 0xeb, 0x09, 0x00, 0x00, 0x00, 0x09, 0x00, 0x20, 0x12, 0x0a, 0x00, 0x00, 0x00, 0xf7, 0xff,
    0x30, 0x39, 0x0a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x60, 0x0a, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x50, 0x87, 0x0a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, 0xae, 0x0a, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x70, 0xd5, 0x0a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0xfc, 0x0a, 0x00, 0x00, 0x00, 0x09, 0x00,
    0x90, 0x23, 0x0b, 0x00, 0x00, 0x00, 0xf7, 0xff, 0xa0, 0x4a, 0x0b, 0x00, 0x00, 0x00, 0x09, 0x00,
    0xb0, 0x71, 0x0b, 0x00, 0x00, 0x00, 0xee, 0xff, 0xc0, 0x98, 0x0b, 0x00, 0x00, 0x00, 0xe5, 0xff,
    0xd0, 0xbf, 0x0b, 0x00, 0x02, 0x00, 0xee, 0xff, 0xe0, 0xe6, 0x0b, 0x00, 0x00, 0x00, 0xee, 0xff,
    0xf0, 0x0d, 0x0c, 0x00, 0x02, 0x00, 0xee, 0xff, 0x00, 0x35, 0x0c, 0x00, 0x00, 0x00, 0xee, 0xff,
    0x10, 0x5c, 0x0c, 0x00, 0x00, 0x00, 0xee, 0xff, 0x20, 0x83, 0x0c, 0x00, 0x01, 0x00, 0xee, 0xff,
    0x30, 0xaa, 0x0c, 0x00, 0x00, 0x00, 0xe5, 0xff, 0x40, 0xd1, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x50, 0xf8, 0x0c, 0x00, 0x02, 0x00, 0xe5, 0xff, 0x60, 0x1f, 0x0d, 0x00, 0x00, 0x00, 0xe5, 0xff,
    0x70, 0x46, 0x0d, 0x00, 0x01, 0x00, 0xe5, 0xff, 0x80, 0x6d, 0x0d, 0x00, 0x00, 0x00, 0xdc, 0xff,
    0x90, 0x94, 0x0d, 0x00, 0x00, 0x00, 0xe5, 0xff, 0xa0, 0xbb, 0x0d, 0x00, 0x00, 0x00, 0x00, 0x00,
    0xb0, 0xe2, 0x0d, 0x00, 0x01, 0x00, 0xee, 0xff, 0xc0, 0x09, 0x0e, 0x00, 0x01, 0x00, 0xe5, 0xff,
    0xd0, 0x30, 0x0e, 0x00, 0x00, 0x00, 0xdc, 0xff, 0xe0, 0x57, 0x0e, 0x00, 0x00, 0x00, 0xe5, 0xff,
    0xf0, 0x7e, 0x0e, 0x00, 0x00, 0x00, 0xe5, 0xff, 
};


static Byte UT200_IQtable[] = {
    0x49, 0x44, 0x3a, 0x39, 0x39, 0x39, 0x00, 0x01, 0x56, 0x3a, 0x02, 0x01, 0x00, 0x00, 0x00, 0x5c,
    0x50, 0xc3, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x60, 0xea, 0x00, 0x00, 0x00, 0x00, 0xf9, 0xff,
    0x70, 0x11, 0x01, 0x00, 0xff, 0xff, 0x00, 0x00, 0x80, 0x38, 0x01, 0x00, 0xff, 0xff, 0x05, 0x00,
    0x90, 0x5f, 0x01, 0x00, 0x00, 0x00, 0xfe, 0xff, 0xa0, 0x86, 0x01, 0x00, 0x00, 0x00, 0x07, 0x00,
    0xb0, 0xad, 0x01, 0x00, 0xff, 0xff, 0x09, 0x00, 0xc0, 0xd4, 0x01, 0x00, 0xff, 0xff, 0x09, 0x00,
    0xd0, 0xfb, 0x01, 0x00, 0xff, 0xff, 0x05, 0x00, 0xe0, 0x22, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00,
    0xf0, 0x49, 0x02, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x71, 0x02, 0x00, 0xff, 0xff, 0x07, 0x00,
    0x10, 0x98, 0x02, 0x00, 0xff, 0xff, 0x0e, 0x00, 0x20, 0xbf, 0x02, 0x00, 0x00, 0x00, 0x0e, 0x00,
    0x30, 0xe6, 0x02, 0x00, 0xff, 0xff, 0x09, 0x00, 0x40, 0x0d, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x50, 0x34, 0x03, 0x00, 0xff, 0xff, 0xce, 0xff, 0x60, 0x5b, 0x03, 0x00, 0x00, 0x00, 0xc5, 0xff,
    0x70, 0x82, 0x03, 0x00, 0x00, 0x00, 0xce, 0xff, 0x80, 0xa9, 0x03, 0x00, 0xff, 0xff, 0xcc, 0xff,
    0x90, 0xd0, 0x03, 0x00, 0xff, 0xff, 0xae, 0xff, 0xa0, 0xf7, 0x03, 0x00, 0xff, 0xff, 0xc9, 0xff,
    0xb0, 0x1e, 0x04, 0x00, 0xff, 0xff, 0xbe, 0xff, 0xc0, 0x45, 0x04, 0x00, 0x00, 0x00, 0xb5, 0xff,
    0xd0, 0x6c, 0x04, 0x00, 0x00, 0x00, 0xc9, 0xff, 0xe0, 0x93, 0x04, 0x00, 0xff, 0xff, 0xbe, 0xff,
    0xf0, 0xba, 0x04, 0x00, 0xff, 0xff, 0xf2, 0xff, 0x00, 0xe2, 0x04, 0x00, 0xff, 0xff, 0xf7, 0xff,
    0x10, 0x09, 0x05, 0x00, 0xff, 0xff, 0xf5, 0xff, 0x20, 0x30, 0x05, 0x00, 0xff, 0xff, 0xf2, 0xff,
    0x30, 0x57, 0x05, 0x00, 0xfe, 0xff, 0xf7, 0xff, 0x40, 0x7e, 0x05, 0x00, 0xfe, 0xff, 0xec, 0xff,
    0x50, 0xa5, 0x05, 0x00, 0xff, 0xff, 0xf7, 0xff, 0x60, 0xcc, 0x05, 0x00, 0xff, 0xff, 0xec, 0xff,
    0x70, 0xf3, 0x05, 0x00, 0x00, 0x00, 0xf0, 0xff, 0x80, 0x1a, 0x06, 0x00, 0xff, 0xff, 0xf0, 0xff,
    0x90, 0x41, 0x06, 0x00, 0xff, 0xff, 0x2e, 0x00, 0xa0, 0x68, 0x06, 0x00, 0xff, 0xff, 0x3d, 0x00,
    0xb0, 0x8f, 0x06, 0x00, 0xff, 0xff, 0x37, 0x00, 0xc0, 0xb6, 0x06, 0x00, 0xff, 0xff, 0x42, 0x00,
    0xd0, 0xdd, 0x06, 0x00, 0xff, 0xff, 0x2e, 0x00, 0xe0, 0x04, 0x07, 0x00, 0xff, 0xff, 0x4b, 0x00,
    0xf0, 0x2b, 0x07, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x53, 0x07, 0x00, 0xff, 0xff, 0x49, 0x00,
    0x10, 0x7a, 0x07, 0x00, 0xff, 0xff, 0x5b, 0x00, 0x20, 0xa1, 0x07, 0x00, 0xfe, 0xff, 0x4b, 0x00,
    0x30, 0xc8, 0x07, 0x00, 0xff, 0xff, 0x4d, 0x00, 0x40, 0xef, 0x07, 0x00, 0x01, 0x00, 0x47, 0x00,
    0x50, 0x16, 0x08, 0x00, 0xff, 0xff, 0x64, 0x00, 0x60, 0x3d, 0x08, 0x00, 0x00, 0x00, 0x56, 0x00,
    0x70, 0x64, 0x08, 0x00, 0xff, 0xff, 0x66, 0x00, 0x80, 0x8b, 0x08, 0x00, 0xff, 0xff, 0x52, 0x00,
    0x90, 0xb2, 0x08, 0x00, 0xfe, 0xff, 0x44, 0x00, 0xa0, 0xd9, 0x08, 0x00, 0xff, 0xff, 0x59, 0x00,
    0xb0, 0x00, 0x09, 0x00, 0xff, 0xff, 0x56, 0x00, 0xc0, 0x27, 0x09, 0x00, 0xff, 0xff, 0x66, 0x00,
    0xd0, 0x4e, 0x09, 0x00, 0xff, 0xff, 0x62, 0x00, 0xe0, 0x75, 0x09, 0x00, 0xff, 0xff, 0x94, 0x00,
    0xf0, 0x9c, 0x09, 0x00, 0x00, 0x00, 0x92, 0x00, 0x00, 0xc4, 0x09, 0x00, 0xfe, 0xff, 0x82, 0x00,
    0x10, 0xeb, 0x09, 0x00, 0xff, 0xff, 0x52, 0x00, 0x20, 0x12, 0x0a, 0x00, 0xff, 0xff, 0x98, 0x00,
    0x30, 0x39, 0x0a, 0x00, 0x00, 0x00, 0x59, 0x00, 0x40, 0x60, 0x0a, 0x00, 0x00, 0x00, 0x79, 0x00,
    0x50, 0x87, 0x0a, 0x00, 0x00, 0x00, 0x9d, 0x00, 0x60, 0xae, 0x0a, 0x00, 0xff, 0xff, 0x9d, 0x00,
    0x70, 0xd5, 0x0a, 0x00, 0xff, 0xff, 0x8b, 0x00, 0x80, 0xfc, 0x0a, 0x00, 0xff, 0xff, 0xa2, 0x00,
    0x90, 0x23, 0x0b, 0x00, 0xff, 0xff, 0x64, 0x00, 0xa0, 0x4a, 0x0b, 0x00, 0xff, 0xff, 0x9d, 0x00,
    0xb0, 0x71, 0x0b, 0x00, 0xff, 0xff, 0x89, 0x00, 0xc0, 0x98, 0x0b, 0x00, 0xff, 0xff, 0x89, 0x00,
    0xd0, 0xbf, 0x0b, 0x00, 0x00, 0x00, 0x89, 0x00, 0xe0, 0xe6, 0x0b, 0x00, 0xff, 0xff, 0x8b, 0x00,
    0xf0, 0x0d, 0x0c, 0x00, 0xff, 0xff, 0x9f, 0x00, 0x00, 0x35, 0x0c, 0x00, 0xfe, 0xff, 0x8f, 0x00,
    0x10, 0x5c, 0x0c, 0x00, 0x01, 0x00, 0x98, 0x00, 0x20, 0x83, 0x0c, 0x00, 0x00, 0x00, 0x84, 0x00,
    0x30, 0xaa, 0x0c, 0x00, 0xff, 0xff, 0xb1, 0x00, 0x40, 0xd1, 0x0c, 0x00, 0x00, 0x00, 0xb4, 0x00,
    0x50, 0xf8, 0x0c, 0x00, 0x00, 0x00, 0xad, 0x00, 0x60, 0x1f, 0x0d, 0x00, 0xff, 0xff, 0x9f, 0x00,
    0x70, 0x46, 0x0d, 0x00, 0x00, 0x00, 0xc1, 0x00, 0x80, 0x6d, 0x0d, 0x00, 0xff, 0xff, 0xb8, 0x00,
    0x90, 0x94, 0x0d, 0x00, 0xff, 0xff, 0xcd, 0x00, 0xa0, 0xbb, 0x0d, 0x00, 0xff, 0xff, 0xcb, 0x00,
    0xb0, 0xe2, 0x0d, 0x00, 0xff, 0xff, 0xc6, 0x00, 0xc0, 0x09, 0x0e, 0x00, 0xff, 0xff, 0xda, 0x00,
    0xd0, 0x30, 0x0e, 0x00, 0xff, 0xff, 0xc8, 0x00, 0xe0, 0x57, 0x0e, 0x00, 0xff, 0xff, 0xd4, 0x00,
    0xf0, 0x7e, 0x0e, 0x00, 0xfe, 0xff, 0xd1, 0x00, 0xc0, 0xc6, 0x2d, 0x00, 0xfe, 0xff, 0xd1, 0x00,
};

static int init_modulator(
	int frequency, 
	int bandwidth, 
	int gain, 
	int constellation, 
	int codeRate, 
	int guardInterval, 
	int transmissionMode, 
	int cellid
)
{
    TPS tps;
    int outgainvalue;
    Dword dwError = ERR_NO_ERROR;
    MODULATION_PARAM ChannelModulation_Setting;

    /* Open handle Power On and Check device information */
    if((dwError = g_ITEAPI_TxDeviceInit(DevType, DevNo)) != ERR_NO_ERROR) {
        printf("g_ITEAPI_TxDeviceInit fail\n");
        return 0;
    }

    if (DevType == EAGLEII) {
        dwError = g_ITEAPI_TxSetIQTable(UT200_IQtable, sizeof(UT200_IQtable), DevNo);
        if (dwError) {
            fprintf(stderr, "Set IQ Calibration Table fail: 0x%08lx\n", dwError);
        }
    } else {
        dwError = g_ITEAPI_TxSetIQTable(UT100_IQtable, sizeof(UT100_IQtable), DevNo);
        if (dwError) {
            fprintf(stderr, "Set IQ Calibration Table fail: 0x%08lx\n", dwError);
        }
    }

    if ((dwError = g_ITEAPI_TxSetChannel(frequency, bandwidth, DevNo)) != ERR_NO_ERROR) {
	fprintf(stderr, "g_ITEAPI_TxSetChannel error\n");
	return 0;
    }

    if(g_ITEAPI_TxAdjustOutputGain(gain, &outgainvalue, DevNo) != ERR_NO_ERROR) {
	fprintf(stderr, "g_ITEAPI_TxAdjustOutputGain error\n");
    }


    ChannelModulation_Setting.constellation = constellation;
    ChannelModulation_Setting.highCodeRate = codeRate;
    ChannelModulation_Setting.interval = guardInterval;
    ChannelModulation_Setting.transmissionMode = transmissionMode;
	
    if ((dwError = g_ITEAPI_TxSetChannelModulation(ChannelModulation_Setting, DevNo)) != ERR_NO_ERROR)
	    fprintf(stderr, "g_ITEAPI_TxSetChannelModulation error!!, %lu ******\n", dwError);

    dwError = g_ITEAPI_TxGetTPS(&tps, DevNo);
    if (dwError)   {
               fprintf(stderr, "\n****** g_ITEAPI_TxGetTPS error!!, %lu ******\n", dwError);
    }
    tps.cellid = cellid;

    dwError = g_ITEAPI_TxSetTPS(tps, DevNo);
    if (dwError)   {
               fprintf(stderr, "\n****** g_ITEAPI_TxSetTPS error!!, %lu ******\n", dwError);
    }

    g_ITEAPI_TxSetModeEnable(True, DevNo);
    g_ITEAPI_StartTransfer(DevNo);

    return 0;
}

static void ts_output(unsigned char *p, int total)
{
	int len;
	len = g_ITEAPI_TxSendTSData(p, total, DevNo);
again:
	if (len < 0) {
		if (errno == EWOULDBLOCK || errno == EAGAIN) {
			logwrite(LOG_ERR, "Blocking write");
			goto again;
		} else {
			logwrite(LOG_ERR, "Sending failed: %", errno);
		}
	}
}

