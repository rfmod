#ifndef	_MSOCKET_H_

#define _MSOCKET_H_

int socket_open(const char *laddr, int port, const char *ifname, int ifindex, unsigned int buffer_size, int timeout);
int socket_connect(const char *laddr, const char *raddr, int port, int ifindex, unsigned int buffer_size, int nonblock, int ttl, int dscp);
void socket_close(int sock);

#endif