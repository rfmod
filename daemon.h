#ifndef	_DAEMON_H_

#define _DAEMON_H_

void daemonize(const char *lockfile, const char *username);

#endif
