#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <sys/param.h>
#include <time.h>
#include <sys/time.h>
#include <syslog.h>

#include "logging.h"

int loglevel=LOG_ERR;

void logwrite_inc_level() {
	loglevel++;
}

void logwrite_dec_level() {
	if (loglevel > LOG_ERR) {
		loglevel--;
	}
}

void logwrite(int level, const char *format, ...) {
	va_list		pvar;

	if (level > loglevel)
		return;

	va_start (pvar, format);
	vsyslog(level, format, pvar);
	va_end (pvar);
}
