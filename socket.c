#define	_GNU_SOURCE

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netdb.h>
#include <time.h>
#include <syslog.h>
#include <errno.h>

#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

#include "logging.h"

void socket_close(int sock) {
	close(sock);
}

int socket_open(const char *laddr, int port, const char *ifname, int ifindex, unsigned int buffer_size, int timeout) {
	struct addrinfo hints;
	struct addrinfo *res;
	char portstr[8];

	int			sock;
	int			enable = 1;
	int			bufsize;
	int			option;
	int 		error;
	socklen_t		olen;


	memset(&hints, 0, sizeof(hints));
	snprintf(portstr, sizeof(portstr), "%d", port);
	hints.ai_family = PF_UNSPEC;
	hints.ai_flags = AI_PASSIVE | AI_NUMERICSERV;
	hints.ai_socktype = SOCK_DGRAM;

	error = getaddrinfo(laddr, portstr, &hints, &res);
	if (error) {
		logwrite(LOG_ERR, gai_strerror(error));
		return -1;
	}

	sock=socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (sock<0)
		return sock;
#ifdef SO_REUSEADDR
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof (enable));
#endif

#if defined(SO_RCVBUF) || defined(SO_RCVBUFFORCE)
	/* Increase the receive buffer size to 1/2MB (8Mb/s during 1/2s)
	 * to avoid packet loss caused in case of scheduling hiccups */
#if defined(SO_RCVBUFFORCE)
	bufsize = (int)buffer_size;
	setsockopt(sock, SOL_SOCKET, SO_RCVBUFFORCE, (void*)&bufsize, sizeof(bufsize));
	olen = sizeof(option);
	getsockopt(sock, SOL_SOCKET, SO_RCVBUF, &option, &olen);
#else
	bufsize = (int)buffer_size;
	setsockopt(sock, SOL_SOCKET, SO_RCVBUF, (void*)&bufsize, sizeof(bufsize));
	olen = sizeof(option);
	getsockopt(sock, SOL_SOCKET, SO_RCVBUF, &option, &olen);
#error SO_RCVBUFFORCE ?
#endif
#else
#error SO_RCVBUF?
#endif

	if (bind(sock, res->ai_addr, res->ai_addrlen) != 0) {
		close(sock);
		freeaddrinfo(res);
		return -1;
	}

	switch (res->ai_family) {
		case AF_INET: {
			struct sockaddr_in *sin = (struct sockaddr_in *)res->ai_addr;
			if (IN_MULTICAST(ntohl(sin->sin_addr.s_addr))) {
				struct ip_mreqn	mreq;
				memset(&mreq, 0, sizeof(struct ip_mreqn));
				mreq.imr_multiaddr = sin->sin_addr;
				mreq.imr_ifindex = ifindex;
				if (setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq))) {
					logwrite(LOG_ERR, "Error joining multicast group");
				}
			}
#ifdef SO_BINDTODEVICE
            if (ifname && IN_MULTICAST(ntohl(sin->sin_addr.s_addr))) {
                if ( setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE, ifname, strlen(ifname)+1 ) < 0 ) {
                    logwrite(LOG_ERR, "couldn't bind to device %s (%s)", ifname, strerror(errno) );
                }
            }
#endif
			break;
		}
		case AF_INET6: {
			struct sockaddr_in6 *sin6 = (struct sockaddr_in6 *)res->ai_addr;
			if (IN6_IS_ADDR_MULTICAST(&sin6->sin6_addr)) {
				struct ipv6_mreq imr;
				memset(&imr, 0, sizeof(struct ipv6_mreq));
				imr.ipv6mr_interface = ifindex;
				imr.ipv6mr_multiaddr = sin6->sin6_addr;
				if (setsockopt(sock, IPPROTO_IPV6, IPV6_ADD_MEMBERSHIP, &imr, sizeof(imr))) {
					logwrite(LOG_ERR, "Error joining IPV6 multicast group");
				}
			}
			break;
		}
		default:
			logwrite(LOG_ERR, "Family %d?", res->ai_family);
	}

	if (timeout) {
		struct timeval tv;
		tv.tv_sec = timeout/1000;
		tv.tv_usec = 0;
		setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));
	}

	freeaddrinfo(res);
	return sock;
}

int socket_connect(const char *laddr, const char *raddr, int port, int ifindex, unsigned int buffer_size, int nonblock, int ttl, int dscp)
{
	struct addrinfo hints;
	struct addrinfo *res;
	struct addrinfo *res2;
	char portstr[8];
	int error;
	int sock;
	int			enable = 1;
	int			bufsize;
	int			option;
	socklen_t		olen;

	memset(&hints, 0, sizeof(hints));
	snprintf(portstr, sizeof(portstr), "%d", port);
	hints.ai_family = PF_UNSPEC;
	hints.ai_flags = AI_NUMERICSERV;
	hints.ai_socktype = SOCK_DGRAM;

	error = getaddrinfo(raddr, portstr, &hints, &res);
	if (error) {
		logwrite(LOG_ERR, "socket_connect: remoteaddr: %s", gai_strerror(error));
		return -1;
	}

	sock=socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (sock<0)
		return sock;
#ifdef SO_REUSEADDR
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof (enable));
#endif

#if defined(SO_SNDBUF) || defined(SO_SNDBUFFORCE)
	/* Increase the receive buffer size to 1/2MB (8Mb/s during 1/2s)
	 * to avoid packet loss caused in case of scheduling hiccups */
#if defined(SO_SNDBUFFORCE)
	bufsize = (int)buffer_size;
	setsockopt(sock, SOL_SOCKET, SO_SNDBUFFORCE, (void*)&bufsize, sizeof(bufsize));
	olen = sizeof(option);
	getsockopt(sock, SOL_SOCKET, SO_SNDBUF, &option, &olen);
#else
	bufsize = (int)buffer_size;
	setsockopt(sock, SOL_SOCKET, SO_SNDBUF, (void*)&bufsize, sizeof(bufsize));
	olen = sizeof(option);
	getsockopt(sock, SOL_SOCKET, SO_SNDBUF, &option, &olen);
#error SO_SNDBUFFORCE ?
#endif
#else
#error SO_SNDBUF?
#endif

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = res->ai_family;
	hints.ai_flags = AI_PASSIVE | AI_NUMERICSERV;
	hints.ai_socktype = SOCK_DGRAM;

	error = getaddrinfo(laddr, "0", &hints, &res2);
	if (error) {
		logwrite(LOG_ERR, "socket_connect: localaddr [%s]: %s", laddr, gai_strerror(error));
		return -1;
	}


	if (bind(sock, res2->ai_addr, res2->ai_addrlen) != 0) {
		close(sock);
		freeaddrinfo(res);
		freeaddrinfo(res2);
		return -1;
	}

	if (connect(sock, res->ai_addr, res->ai_addrlen) < 0) {
		return -1;
	}

	if (ifindex) {
		switch (res->ai_family) {
			case AF_INET: {
				struct ip_mreqn iface_struct;
				iface_struct.imr_multiaddr.s_addr = INADDR_ANY;
				iface_struct.imr_address.s_addr = INADDR_ANY;
				iface_struct.imr_ifindex = ifindex;
				if (setsockopt(sock, IPPROTO_IP, IP_MULTICAST_IF, &iface_struct, sizeof (struct ip_mreqn)) < 0) {
					return -1;
				}
				break;
			}
			case AF_INET6: {
				if (setsockopt(sock, IPPROTO_IPV6, IPV6_MULTICAST_IF, &ifindex, sizeof(ifindex)) < 0) {
					return -1;
				}
			}
		}
	}

	if (ttl) {
		switch (res->ai_family) {
			case AF_INET: {
				if (setsockopt(sock, IPPROTO_IP, IP_MULTICAST_TTL, &ttl, sizeof(ttl)) < 0) {
					return -1;
				}
				break;
			}
			case AF_INET6: {
				if (setsockopt(sock, IPPROTO_IPV6, IPV6_MULTICAST_HOPS, &ttl, sizeof(ttl)) < 0) {
					return -1;
				}
			}
		}
	}
	if (dscp) {
		switch (res->ai_family) {
			case AF_INET: {
				if (setsockopt(sock, IPPROTO_IP, IP_TOS, &dscp, sizeof(dscp)) < 0) {
					return -1;
				}
				break;
			}
			case AF_INET6: {
				/* FIXME: currently unsupported */
			}
		}
	}

	return sock;
}
